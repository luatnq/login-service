package com.example.demo.service.impl;

import com.example.demo.caches.CacheManagers;
import com.example.demo.dto.AccountDTO;
import com.example.demo.dto.FormData;
import com.example.demo.dto.Profile;
import com.example.demo.dto.UserDTO;
import com.example.demo.entity.*;
import com.example.demo.exception.DataNotProperlyFormatException;
import com.example.demo.exception.UserAlreadyExistsException;
import com.example.demo.exception.UserNonActiveException;
import com.example.demo.exception.UserNotFoundException;
import com.example.demo.mapper.MapperUser;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.RandomStringUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.temporal.ChronoField;
import java.util.Objects;
import java.util.concurrent.TimeUnit;


@Service
@Transactional
@Slf4j
public class UserServiceImp implements UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private MapperUser mapperUser;
    @Autowired
    private CacheManagers cacheManagers;

    @Override
    /**
     * @Description
     *
     */

    public FormData registration(UserDTO userDTO) {
        checkUserAlreadyExists(userDTO.getUsername());
        validate(userDTO);
        User user = convertToUser(userDTO);
        user.setIsActive(0);
        saveToDB(user);
        return throwData(Message.REGISTRATION_SUCCESSFUL);
    }

    @Override
    public Token login(AccountDTO accountDTO) {
        User user = checkExist(accountDTO.getUserName());
        Token token = generatorToken(user);
        cacheManagers.setValue(token.getAccessToken(), user.getUserName());
        return token;
    }

    @Override
    public User findUserByUserName(String userName) {
        return userRepository.findByUserName(userName).orElse(null);
    }

    @Override
    public FormData active(AccountDTO accountDTO) {
        User user = checkExist(accountDTO.getUserName());
        if (user.getIsActive() == 0) {
            user.setIsActive(1);
            saveToDB(user);
        }
        return throwData(Message.ACTIVE_SUCCESSFUL);
    }

    @Override
    public Profile getProfile(String token) {
        User user = getDataFromCache(token);
        return convertToUserDto(user);
    }

    private Token generatorToken(User user) {
        checkActive(user);
        Token token = new Token();
        long expiryDate = generatorExpiryDate();
        token.setAccessToken(generatorAccessToken());
        token.setExpiration(expiryDate);
        return token;
    }

    private long generatorExpiryDate() {
        long expiryDate = LocalDateTime.now().getLong(ChronoField.EPOCH_DAY) * 24 +
                TimeUnit.SECONDS.toHours(cacheManagers.timeToLive);
        return expiryDate;
    }

    private String generatorAccessToken() {
        return RandomStringUtils.random(40, true, true);
    }

    private void saveToDB(User user) {
        userRepository.save(user);
    }

    private User convertToUser(UserDTO userDTO) {
        return mapperUser.dtoResponseUser(userDTO);
    }

    private Profile convertToUserDto(User user) {
        return mapperUser.userResponseDto(user);
    }

    private FormData throwData(String message) {
        FormData formData = new FormData();
        formData.setMessage(message);
        formData.setTime(LocalDateTime.now());
        return formData;
    }

    private User getDataFromCache(String token) {
        String userName = cacheManagers.get(token);
        return checkExist(userName);
    }

    private User checkExist(String userName) {
        User user = findUserByUserName(userName);
        if (Objects.isNull(user)) {
            throw new UserNotFoundException();
        }
        return user;
    }

    private void checkActive(User user) {
        if (user.getIsActive() == 0) {
            throw new UserNonActiveException();
        }
    }

    private void checkUserAlreadyExists(String userName) {
        User user = findUserByUserName(userName);
        if (Objects.nonNull(user)) {
            throw new UserAlreadyExistsException();
        }
    }

    private void validate(UserDTO userDTO) {
        if (!VerifyUserName.validateUserName(userDTO.getUsername())) {
            throw new DataNotProperlyFormatException(userDTO.getUsername());
        }
        if (!VerifyPassword.validatePassword(userDTO.getPassword())) {
            throw new DataNotProperlyFormatException(userDTO.getPassword());
        }
        if (!VerifyEmail.validateEmail(userDTO.getEmail())) {
            throw new DataNotProperlyFormatException(userDTO.getEmail());
        }
    }
}
