package com.example.demo.service;

import com.example.demo.dto.AccountDTO;
import com.example.demo.dto.FormData;
import com.example.demo.dto.Profile;
import com.example.demo.dto.UserDTO;
import com.example.demo.entity.Token;
import com.example.demo.entity.User;
import com.example.demo.exception.UserNotFoundException;
import com.fasterxml.jackson.core.JsonProcessingException;

public interface UserService {
    FormData registration(UserDTO userDTO);

    Token login(AccountDTO accountDTO) throws JsonProcessingException, UserNotFoundException;

    User findUserByUserName(String userName);

    FormData active(AccountDTO accountDTO) throws UserNotFoundException;

    Profile getProfile(String token);

}
