package com.example.demo.exception;

import com.example.demo.dto.FormData;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;

@org.springframework.web.bind.annotation.ControllerAdvice
public class ControllerAdvisor extends ResponseEntityExceptionHandler {
    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<?> handleUserNotFoundException(
            UserNotFoundException e, WebRequest request) {
        FormData formData = setFormatData(e.getMessage());
        return new ResponseEntity<>(formData, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(UserNonActiveException.class)
    public ResponseEntity<?> handleUserNonActiveException(
            UserNonActiveException e, WebRequest request) {
        FormData formData = setFormatData(e.getMessage());
        return new ResponseEntity<>(formData, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(DataNotProperlyFormatException.class)
    public ResponseEntity<?> handleDataNotProperlyFormatException(
            DataNotProperlyFormatException e, WebRequest request) {
        FormData formData = setFormatData(e.getMessage());
        return new ResponseEntity<>(formData, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UserAlreadyExistsException.class)
    public ResponseEntity<?> handleUserAlreadyExistsException(
            UserAlreadyExistsException e, WebRequest request) {
        FormData formData = setFormatData(e.getMessage());
        return new ResponseEntity<>(formData, HttpStatus.BAD_REQUEST);
    }

    private FormData setFormatData(String message) {
        FormData formData = new FormData();
        formData.setMessage(message);
        formData.setTime(LocalDateTime.now());
        return formData;
    }
}
