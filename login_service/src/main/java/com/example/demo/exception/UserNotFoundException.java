package com.example.demo.exception;

import com.example.demo.entity.Message;
import lombok.Data;

@Data
public class UserNotFoundException extends RuntimeException {

    public UserNotFoundException() {
        super(Message.USER_NOT_FOUND_EXCEPTION);
    }

}
