package com.example.demo.exception;

import com.example.demo.entity.Message;

public class DataNotProperlyFormatException extends RuntimeException{

    public DataNotProperlyFormatException(String str){
        super(str+" "+ Message.DATA_NOT_PROPERLY_FORMAT_EXCEPTION);
    }
}
