package com.example.demo.exception;

import com.example.demo.entity.Message;

public class UserAlreadyExistsException extends RuntimeException{
    public UserAlreadyExistsException(){
        super(Message.USER_ALREADY_EXISTS);
    }
}
