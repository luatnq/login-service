package com.example.demo.exception;

import com.example.demo.entity.Message;

public class UserNonActiveException extends RuntimeException {

    public UserNonActiveException() {
        super(Message.USER_NON_ACTIVE_EXCEPTION );
    }
}
