package com.example.demo.entity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VerifyUserName {
    private static final Pattern VALID_USERNAME_REGEX =
            Pattern.compile("^[a-zA-Z0-9]([._](?![._])|[a-zA-Z0-9]){3,18}[a-zA-Z0-9]$", Pattern.CASE_INSENSITIVE);

    public static boolean validateUserName(String userNameStr) {
        Matcher matcher = VALID_USERNAME_REGEX.matcher(userNameStr);
        return matcher.find();
    }
}
