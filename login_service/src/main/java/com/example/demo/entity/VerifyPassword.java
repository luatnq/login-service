package com.example.demo.entity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VerifyPassword {
    private static final Pattern VALID_PASSWORD_REGEX =
            Pattern.compile("^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$", Pattern.CASE_INSENSITIVE);

    public static boolean validatePassword(String passwordStr) {
        Matcher matcher = VALID_PASSWORD_REGEX.matcher(passwordStr);
        return matcher.find();
    }
}
