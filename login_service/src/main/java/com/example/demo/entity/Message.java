package com.example.demo.entity;

public class Message {
    public static String DATA_NOT_PROPERLY_FORMAT_EXCEPTION = "wrong format";
    public static String USER_NON_ACTIVE_EXCEPTION = "User non active";
    public static String USER_NOT_FOUND_EXCEPTION = "User not found";
    public static String REGISTRATION_SUCCESSFUL = "Registration successful";
    public static String ACTIVE_SUCCESSFUL = "Active successful";
    public static String USER_ALREADY_EXISTS = "User already exists";
    public static String LOGIN_SUCCESSFUL = "Login successful";
    public static String GET_PROFILE_SUCCESSFUL = "Get profile successful";
}
