package com.example.demo.entity;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "user")
@Builder
public class User implements Serializable {

    @Id
    private String id;

    @Field(name = "user_name")
    private String userName; 

    private String password;

    @Field(name = "is_active")
    private int isActive;

    private String email;

    @Field(name = "full_name")
    private String fullName; 

    private int age;


}
