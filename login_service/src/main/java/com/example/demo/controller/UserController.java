package com.example.demo.controller;

import com.example.demo.dto.AccountDTO;
import com.example.demo.dto.FormData;
import com.example.demo.dto.Profile;
import com.example.demo.dto.UserDTO;
import com.example.demo.entity.Message;
import com.example.demo.entity.Token;
import com.example.demo.service.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequestMapping("/api/v1/users")
public class UserController {
    @Autowired
    private UserService userService;

    @PostMapping(produces = "application/json")
    public ResponseEntity<?> registration(@RequestBody UserDTO userDTO) {
        FormData formData = userService.registration(userDTO);
        log.info(Message.REGISTRATION_SUCCESSFUL, userDTO);
        return new ResponseEntity<>(formData, HttpStatus.OK);
    }

    @PostMapping(path = "/login", produces = "application/json")
    public ResponseEntity<?> login(@RequestBody AccountDTO accountDTO) throws JsonProcessingException {
        Token token = userService.login(accountDTO);
        log.info(Message.LOGIN_SUCCESSFUL);
        return new ResponseEntity<>(token, HttpStatus.OK);
    }

    @PutMapping(produces = "application/json")
    public ResponseEntity<?> active(@RequestBody AccountDTO accountDTO) {
        FormData formData = userService.active(accountDTO);
        log.info(Message.ACTIVE_SUCCESSFUL);
        return new ResponseEntity<>(formData, HttpStatus.OK);
    }

    @GetMapping(produces = "application/json")
    public ResponseEntity<?> getProfile(@RequestHeader String token) {
        Profile profile = userService.getProfile(token);
        log.info(Message.GET_PROFILE_SUCCESSFUL, profile);
        return new ResponseEntity<>(profile, HttpStatus.OK);
    }
}
