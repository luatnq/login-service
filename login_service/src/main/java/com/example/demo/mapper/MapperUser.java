package com.example.demo.mapper;

import com.example.demo.dto.Profile;
import com.example.demo.dto.UserDTO;
import com.example.demo.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface MapperUser {
    @Mappings({
            @Mapping(target = "id", source = "id"),
            @Mapping(target = "userName", source = "username")
    })
    User dtoResponseUser(UserDTO userDTO);

    Profile userResponseDto(User user);
}
