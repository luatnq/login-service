package com.example.demo.caches;

import com.example.demo.exception.UserNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
@Slf4j
public class CacheManagers {
    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    public final long timeToLive = 300;

    public void setValue(String key, String userName) throws UserNotFoundException {

        redisTemplate.opsForValue().set(key, userName, timeToLive, TimeUnit.SECONDS);

    }

    public String get(String key) {
        try {
            String result = redisTemplate.opsForValue().get(key);
            log.info(result);
            return result;
        } catch (Exception e) {
            return null;
        }
    }

    public void delete(String key) {
        redisTemplate.delete(key);
    }
}
